'use strict';

let jlib;
(function () {

let jcore = {};
let jtypes = Object.create(jcore);
let defaults = {};
let internals = {
    types: {
        '[object Array]'              : 'Array',
        '[object String]'             : 'String',
        '[object Object]'             : 'Object',
        '[object Arguments]'          : 'Arguments',
        '[object Undefined]'          : 'Undefined',
        '[object Number]'             : 'Number',
        '[object Function]'           : 'Function',
        '[object RegExp]'             : 'Regex',
        '[object Boolean]'            : 'Boolean',
        '[object Null]'               : 'Null',
        '[object Symbol]'             : 'Symbol',
        '[object Error]'              : 'Error',
        '[object Date]'               : 'Date',
        '[object GeneratorFunction]'  : 'GeneratorFunction',
        '[object Map]'                : 'Map',
        '[object Set]'                : 'Set',
        '[object WeakMap]'            : 'WeakMap'
    }
};

//MAGIC HAPPENS HERE!
jlib = function(iterator) {
    let type = jlib.typer(iterator);
    let j;

    j = Object.create(jtypes[type]);
    j.iterator = jlib.clone(iterator);
    j.type = type;

    return j;
};

jlib.clone = function(obj){
    return JSON.parse(JSON.stringify(obj));
};

jlib.typer = function(obj) 
    { return internals.types[ Object.prototype.toString.call(obj) ]; };

jlib.interpolateArgs = function(func) {
    let deps = func.toString().match(/^function\s*[^\(]*\(\s*([^\)]*)\)/m);
    deps = deps && deps.length ? deps[1] : [];

    return deps.length ? deps.replace(/ /g, '').split(',') : [];
};

jlib.defaults = function(update) {
    if (!update) return defaults;
    jlib.extend(defaults, update);
    return defaults;
};

jlib.types = function(update) {
    if (!update) return internals.types;
    jlib.extend(internals.types, update);
    return internals.types;
};

jlib.enhance = function(name, types, func, core) {
    if (!name) throw 'A name and a function are required';
    let args = [];
    let typeDeps = [];
    let key;
    let i;

    if (jlib.typer(types) === 'Function') {
        core = func;
        func = types;
        types = null;
    } else {
        core = false;
    }
    for (key in internals.types) {
        if (name === internals.types[key]) throw name + ' function can not be named after a data type';
    }

    //DEPENDENCY INJECTION
    let deps = jlib.interpolateArgs(func);

    for (key in jtypes)
        for (i in jtypes[key])
            typeDeps.push(key+'_'+i);

    for (i = 0; i < deps.length; i++) {
        if ( ~typeDeps.indexOf(deps[i]) ) {
            let depsSplit = deps[i].split('_');
            args.push( jtypes[ depsSplit[0] ][ depsSplit[1] ] );
        } else if ( jlib[deps[i]] ) {
            args.push(jlib[deps[i]]);
        } else {
            throw deps[i] + ' dependency doesnt exsist';
        }
    }
    //END DEPENDENCY INJECTION


    // CORE ENHANCEMENT
    if (core) {
        if (jlib[name]) throw 'jlib.' + name + ' is already a function';
        jlib[name] = func.apply(this, args);
        return this;
    }
    // END CORE ENHANCEMENT

    // PROTO ENHANCMENT
    if (jlib.typer(types) === 'String') types = [types]; 

    if (jlib.typer(types) === 'Array') {
        for (i = types.length - 1; i >= 0; i--) {
            let t = types[i];
            if (!jtypes[t]) jtypes[t] = {};
            if (jtypes[t][name]) throw t + ' ' +name + ' is already a function';

            jtypes[t][name] = func.apply(this, args);
        }
    } else {
        if (jcore[name]) throw name + ' is already a function';
        jcore[name] = func.apply(this, args);
    }
    //END PROTO ENHANCMENT

    return this;
};

// Borrowed this from jQuery JavaScript Library v2.1.4 http://jquery.com/
jlib.extend = function() {
    let options, name, src, copy, copyIsArray, clone,
        target = arguments[0] || {},
        i = 1,
        length = arguments.length,
        deep = false;

    // Handle a deep copy situation
    if ( jlib.typer(target) === "Boolean" ) {
      deep = target;

      // Skip the boolean and the target
      target = arguments[ i ] || {};
      i++;
    }

    // Handle case when target is a string or something (possible in deep copy)
    if ( jlib.typer(target) !== "Object" && jlib.typer(target) !== 'Function' ) target = {}; 

    // Extend jlib itself if only one argument is passed
    if ( i === length ) {
      target = this;
      i--;
    }

    for ( ; i < length; i++ ) {
      // Only deal with non-null/undefined values
      if ( (options = arguments[ i ]) !== null ) {
        // Extend the base object
        for ( name in options ) {
          src = target[ name ];
          copy = options[ name ];

          // Prevent never-ending loop
          if ( target === copy ) continue;

          // Recurse if we're merging plain objects or arrays
          if ( deep && copy && ( jlib.typer(copy) === 'Object' || (copyIsArray = jlib.typer(copy) === 'Array') ) ) {
            if ( copyIsArray ) {
              copyIsArray = false;
              clone = src && jlib.typer(src) === 'Array' ? src : [];

            } else {
              clone = src && jlib.typer(src) === 'Object' ? src : {};
            }

            // Never move original objects, clone them
            target[ name ] = jlib.extend( deep, clone, copy );

            // Don't bring in undefined values
          } else if ( copy !== undefined ) {
            target[ name ] = copy;
          }
        }
      }
    }

    // Return the modified object
    return target;
};

 module.exports = jlib;

})(); 








//ADDONS

jlib.enhance('extend', 'Object', function() {
    return function(obj){
        if (arguments.length > 1) {
            obj = [].slice.call(arguments);
            obj.unshift(this.iterator);
            jlib.extend.apply(this, obj);
        }else{
            jlib.extend(obj);
        }
        return this;
    };
});

jlib.enhance('each', ['Array', 'String', 'Arguments'], function() {
    return function(cb) {
        if (!cb) throw 'Each requires a callback';
        let value;
        let i = 0;
        let length = this.iterator.length;

        for ( ; i < length; i++ ) {
            value = cb.call( this.iterator[ i ], this.iterator[ i ], i );
            if ( value === false ) {
                break;
            }
        }
        return this.iterator;
    };
});

jlib.enhance('each', 'Object', function() {
    return function(cb) {
        if (!cb) throw 'Each requires a callback';
        let index = 0;
        let value;
        let key;
        for (key in this.iterator) {
            value = cb.call(this.iterator[ key ], this.iterator[ key ], key, index );
            index++;
        }
        return value;
    };
});

jlib.enhance('map', 'Array', function(Array_each) {
    return function(cb) {
        let each = Array_each.bind(this);
        let arr = [];
        each(function(v) {
            arr.push(cb(v));
        });
        return arr;
    };
});

jlib.enhance('reduce', 'Array', function(){
    return function(cb, init){
        // let each = Array_each.bind(this.iterator);
        if (this.iterator === null) throw new TypeError('reduce called on null or undefined');
        if (typeof cb !== 'function') throw new TypeError(cb + ' is not a function');

        let obj = Object(this.iterator);
        let len = obj.length >>> 0;
        let num = 0;
        let value;

        if (arguments.length === 2) {
          value = arguments[1];
        } else {
          while (num < len && !(num in obj)) num++;
          if (num >= len) throw new TypeError('Reduce of empty array with no initial value'); 
          value = obj[num++];
        }

        for (; num < len; num++) {
          if (num in obj) value = cb(value, obj[num], num, obj);
        }
        return value;
    };
});


jlib.enhance('queryStringify', 'Object', function(Object_each){
    return function(cb){
        let each = Object_each.bind(this);
        let string = '';

        each(function(v,k,i){
            string += `&${k}=${v}`;
        });
        return string;
    };
});

